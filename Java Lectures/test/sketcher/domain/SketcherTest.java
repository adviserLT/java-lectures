package sketcher.domain;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SketcherTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSetCurrentShape() {
		fail("Not yet implemented");
	}

	@Test
	public void testCreateShape() {
		fail("Not yet implemented");
	}

	@Test
	public void testSelectShape() {
		Sketcher sketcher = new Sketcher();
		sketcher.selectShape(0);
		assertNull(sketcher.getSelectedShape());

		
		Shape shape1 = sketcher.createShape(2, 2);
		sketcher.createShape(4, 4);

		sketcher.selectShape(0);
		assertEquals(sketcher.getSelectedShape(), shape1);

		
		Sketcher sketcher1 = new Sketcher();

		sketcher1.createShape(2, 2);
		Shape shape4 = sketcher1.createShape(4, 4);

		sketcher1.selectShape(1);
		assertEquals(sketcher1.getSelectedShape(), shape4);
		
	}

	@Test
	public void testGetSelectedShape() {
		Sketcher sketcher = new Sketcher();
		Shape shape = sketcher.getSelectedShape();
		assertNull(shape);
		
		sketcher.createShape(2, 2);
		shape = sketcher.getSelectedShape();
		assertNotNull(shape);
	}

	@Test
	public void testDrawAll() {
		fail("Not yet implemented");
	}

}
