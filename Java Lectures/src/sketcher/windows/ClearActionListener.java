package sketcher.windows;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import sketcher.domain.Sketcher;

public class ClearActionListener implements ActionListener {
	
	private Sketcher sketcher;
	private SketcherWindow window;

	public ClearActionListener(Sketcher sketcher, SketcherWindow window) {
		this.sketcher = sketcher;
		this.window = window;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		sketcher.clearShapes();
		window.repaint();
	}

}
