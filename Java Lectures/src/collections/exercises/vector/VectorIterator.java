package collections.exercises.vector;

public class VectorIterator<T> implements Iterator<T>{

	private Vector<T> vector;
	private int i = 0;
	
	public VectorIterator(Vector<T> vector) {
		this.vector = vector;
	}
	
	@Override
	public boolean hasNext() {
		return i < vector.getSize();
	}

	@Override
	public T next() {
		return vector.get(i++);
	}

}
