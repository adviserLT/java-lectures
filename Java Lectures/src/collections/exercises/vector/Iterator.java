package collections.exercises.vector;

public interface Iterator<T> {
	public boolean hasNext();
	public T next();
}
