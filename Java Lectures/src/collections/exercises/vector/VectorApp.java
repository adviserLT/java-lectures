package collections.exercises.vector;

public class VectorApp {

	public static void main(String[] args) {
		System.out.println("***My VECTOR Application***\n"); 

		Vector<String> vector = new Vector<String>();

		for(int i=0;i<200;i++){
			vector.append("MATOM KAD STRINGAS"+i*10);
		}
		

		iterate(vector.iterator());
		iterate(vector.reverseIterator());
		
	}
	
	public static void iterate(Iterator<String> iterator){
		while(iterator.hasNext()){
			System.out.println("Value is " + iterator.next());
		}
	}

}
