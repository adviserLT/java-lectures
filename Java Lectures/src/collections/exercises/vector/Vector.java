package collections.exercises.vector;


public class Vector<T> implements Iterable<T>{

	private Object [] values;
	private final int interval = 5;
	private int size;
	private int capacity;
	
	public Vector() {
		capacity = interval;
		values = new Object[capacity];
		size = 0;
	}
	
	public void append(T value){
		System.out.println("Inserting element " + value + " at index " + size);
		if(size >= capacity)
			grow();
		values[size] = value;
		size++;
	}
	@SuppressWarnings("unchecked")
	public T get(int index){
		return (T)values[index];
	}
	public int getSize(){
		return size;
	}
	public int getCapacity(){
		return capacity;
	}
	private void grow(){
		System.out.println("Growing array from " + capacity + " to " + capacity+interval);
		capacity = capacity + interval;
		Object [] tmp = new Object[capacity];

		//clock_t begin_time = clock();

		for(int i=0; i<size;i++){
			tmp[i] = values[i];
		}

		//cout << "Growing took "  << float( clock () - begin_time ) /  CLOCKS_PER_SEC << " seconds" <<endl <<endl;

		values = tmp;
	}

	@Override
	public Iterator <T> iterator() {
		return new VectorIterator<T>(this);
	}

	@Override
	public Iterator <T> reverseIterator() {
		return new VectorReverseIterator<T>(this);
	}
	
}
