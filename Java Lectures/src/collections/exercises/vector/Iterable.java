package collections.exercises.vector;

public interface Iterable<T> {
	Iterator<T> iterator();
	Iterator<T> reverseIterator();
}
