package collections.exercises.vector;

public class VectorReverseIterator<T> implements Iterator<T>{

	private Vector<T> vector;
	private int i;
	
	public VectorReverseIterator(Vector<T> vector) {
		this.vector = vector;
		i = vector.getSize()-1;
	}
	
	@Override
	public boolean hasNext() {
		return i>=0;
	}

	@Override
	public T next() {
		return vector.get(i--);
	}

}
