package collections;

import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.Vector;

public class CollectionsApp {
	
	public class Entry{
		
	}

	public static void main(String[] args) {
		Set<String> set = new HashSet<String>();
		set.add("A");
		set.add("B");
		set.add("C");
		set.add("C");
		set.add("D");
		set.add("K");
		System.out.println("### AIBĖS ###");
		System.out.println("Ar 'K' yra aibėje? " + set.contains("K"));
		System.out.println("Aibėje yra " + set.size());
		
		Iterator<String> setIterator = set.iterator();
		while (setIterator.hasNext()) {
			System.out.println(setIterator.next());
		}
		
		
		EnumSet<Dice> diceSet = EnumSet.of(Dice.FACE1, Dice.FACE3, Dice.FACE5);
		System.out.println("### AIBĖ ENUM SET ###");
		System.out.println("Ar 'FACE4' yra aibėje? " + diceSet.contains(Dice.FACE4));
		System.out.println("Aibėje yra " + diceSet.size());
		
		Iterator<Dice> diceIterator = diceSet.iterator();
		while (diceIterator.hasNext()) {
			System.out.println(diceIterator.next());
		}
		
		
		Vector<Integer> vector = new Vector<Integer>();
		for(int i=0; i<160; i++){
			vector.add(i+39);
		}
		vector.insertElementAt(123, 3);
		System.out.println("## VEKTORIUS ##");
		System.out.println("Vektoriaus elementų skaičius " + vector.size());
		System.out.println("Vektoriaus talpa " + vector.capacity());
		System.out.println("Elementas 10 yra " + vector.get(10));
		
		
		
		Stack<String> stack = new Stack<String>();
		stack.push("A");
		stack.push("B");
		stack.push("C");
		
		System.out.println("## DĖKLAS ##");
		System.out.println("Viršūnė " + stack.peek());
		
		Iterator<String> stackIterator = stack.iterator();
		while (stackIterator.hasNext()) {
			System.out.println(stackIterator.next());
		}
		System.out.println("Elementų skaičius prieš " + stack.size());
		while(stack.size() > 0){
			System.out.println(stack.pop());
		}
		System.out.println("Elementų skaičius po " + stack.size());
		
		
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("Jonukas", 4);
		map.put("Onutė", 6);
		map.put("Petriukas", 7);
		map.put("Juozukas", 2);
		
		System.out.println("## ATVAIZDAS ##");
		System.out.println("Petro pažymys " + map.get("Petriukas"));
		
		System.out.println("Reikšmės:");
		Collection<Integer> values = map.values();
		Iterator<Integer> mapValueIterator = values.iterator();
		while (mapValueIterator.hasNext()) {
			System.out.println(mapValueIterator.next());
		}
		
		System.out.println("Raktai:");
		Iterator<String> mapKeyIterator = map.keySet().iterator();
		while (mapKeyIterator.hasNext()) {
			System.out.println(mapKeyIterator.next());
		}

		System.out.println("Reikšmės ir raktai:");
		Iterator<Map.Entry<String, Integer>> mapIterator = map.entrySet().iterator();
		while (mapIterator.hasNext()) {
			Map.Entry<String, Integer> entry = mapIterator.next();
			System.out.println(entry.getKey() + " " + entry.getValue());
		}
	}

}
