package box;

public class BoxApp {

	public static void main(String[] args) {
		
		Box box1 = new Box(10,20,15);
		Box box2 = new Box(1,2,5);
		
		System.out.println(new Box(5,7,8));
		System.out.println(box2);
	
		box1.setDepth(34);
		box2.setDepth(234);
		
		System.out.println(box1);
		System.out.println(box2);
		
		System.out.println("Radau tūrio reikšmę " + box2.getVolume());
		
	}

}
