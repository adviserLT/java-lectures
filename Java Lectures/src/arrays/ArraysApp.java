package arrays;

/**
 * Arrays declaration and usage example
 * 
 * @author Zilvinas
 *
 */
public class ArraysApp {

	/**
	 * 
	 * @param args Console parameters
	 */
	public static void main(String[] args) {
		
		// Array of chars
		char [] letters = new char[3];
		letters[0] = 'A';
		letters[1] = 'B';
		letters[2] = 'C';
		
		// Array of integers
		int [] numbers = new int[]{1,4,5,6,7};
		
		// Array of strings
		String [] strings = {"red","green","yellow","blue","white"};
	    
		// Array of objects
	    Object [] things = new Object[4];
	    things[0] = 56;
	    things[1] = "penkiasdesimt";
	    things[2] = new Object();

		System.out.println("### Iterating over array of numbers ###");
	    for (int i = 0; i < numbers.length; i++) {
			System.out.println(numbers[i]);
		}
	    
		System.out.println("### Iterating over array of chars ###");
	    for (int i = 0; i < letters.length; i++) {
			System.out.println(letters[i]);
		}

		System.out.println("### Iterating over array of strings ###");
	    for (int i = 0; i < strings.length; i++) {
			System.out.println(strings[i]);
		}

	}
	

}
