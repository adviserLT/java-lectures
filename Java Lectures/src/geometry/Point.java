package geometry;

import java.util.Scanner;

public class Point {
	
	double x;
	double y;
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Point(final Point point){
		this(point.x, point.y);
	}
	
	public void move(double xDelta, double yDelta){
		x += xDelta;
		y += yDelta;
	}
	
	public double distance(final Point p){
		return Math.sqrt(
			(x - p.x) * (x - p.x) +
			(y - p.y) * (y - p.y)
		);
	}
	
	@Override
	public String toString() {
		return x + "," + y;
	}
	
	public double readX1() {
		Scanner point = new Scanner(System.in);
		System.out.println("Iveskite x1 koordinates: ");
		double x1 = point.nextDouble();
		point.close();
		return x1;
		//System.out.println("Iveskite y1 koordinates: ");
		//double y1 = point.nextDouble();
		//System.out.println("Iveskite x2 koordinates: ");
		//double x2 = point.nextDouble();
		//System.out.println("Iveskite y2 koordinates: ");
		//double y2 = point.nextDouble();
	}
	
	//public double readX2(); {
		
	//}
}
