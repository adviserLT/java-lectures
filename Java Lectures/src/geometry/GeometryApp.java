package geometry;

public class GeometryApp {

	public static void main(String[] args) {

		Point a = new Point(5, 5);
		Point b = new Point(10, 15);
		System.out.println("Buvo sukurti du taškai: ("+a+")("+b+") !");
		
		Line l1 = new Line(a, b);
		Line l2 = new Line(10, 23, 5, 15);
		System.out.println("Buvo sukurtos dvi linijos: "+l1+" "+l2+" !");
		System.out.println("l1 ilgis: "+l1.lenght());
		System.out.println("l2 ilgis: "+l2.lenght());
		System.out.println("Linijos kertasi taške: ("+l1.intersects(l2)+")!");
		
		Line l3 = new Line(a, b);
		System.out.println("Linijos kertasi taške: ("+l1.intersects(l3)+")!");
		
		Line l4 = new Line(new Point(a), new Point(b));
		a.move(3, 0);
		b.move(3, 0);
		System.out.println("Linijos kertasi taške: ("+l1.intersects(l4)+")!");
		
	}

}
