package variables.exercises;

import java.util.InputMismatchException;
import java.util.Scanner;

public class GradesApp {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		try {
			int grade = scanner.nextInt();
			System.out.println(getGradeText(grade));
		} catch (InputMismatchException e) {
			System.out.println("klaidingas įvedimas");
		}
		scanner.close();
	}
	
	public static String getGradeText(int grade){
		switch (grade) {
		case 1:
			return "1 (blogai)";
		case 2:
			return "2 (nepatenkinamai)";
		case 3:
			return "3 (silpnai)";
		case 4:
			return "4 (gerai)";
		case 5:
			return "5 (puikiai)";
		default:
			return "vertinimas neatpažintas";
		}
	}

}
