package generics;

public class GenericsApp {

	public static void main(String[] args) {
		Point<Double> pointA = new Point<Double>(2.3, 3.4);
		Point<Integer> pointB = new Point<Integer>(2, 4);
		Point<String> pointC = new Point<String>("A", "B");
		Point<Object> pointD = new Point<Object>(new Object(), new GenericsApp());
		
		System.out.println(pointA);
		System.out.println(pointB);
		System.out.println(pointC);
		System.out.println(pointD);
	}

}
