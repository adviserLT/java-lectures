package windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class MyWindow extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JLabel textLabel = new JLabel(" ");
	private Box head = Box.createVerticalBox();
	
	public MyWindow(String title) {
		super(title);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
	}
	
	public void displayText(String text){
		textLabel.setText(text);
	}
	
	public void appendText(String text){
		head.add(new JLabel(text));
		pack();
		repaint();
	}
	
	public void exit(){
		Object[] options = {"Taip", "Ne", "Galbūt"};
		int option = JOptionPane.showOptionDialog(this, "Ar tikrai uždaryti programą?", "Patvirtinimas", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
		if(option == JOptionPane.YES_OPTION){
			//JColorChooser.showDialog(this, "Pasirink", Color.YELLOW);
			dispose();
			System.exit(0);
		}
		
		
		/*String response = JOptionPane.showInputDialog(this, "Sakyk ką nors!");
			appendText(response);
			dispose();
			System.exit(0);
		*/
	}
	
	public void invokeDialog(String text){
		AboutDialog aboutDialog = new AboutDialog(this, "Meniu monitorius", "Paspaudėte meniu '" + text + "', norėdami išeiti spauskite OK!");
		aboutDialog.setVisible(true);
	}
	
	public void createFlowLayout(){
		
		Container content = getContentPane();
		FlowLayout flow = new FlowLayout();
		flow.setAlignment(FlowLayout.LEFT);
		flow.setVgap(20);
		flow.setHgap(20);
		content.setLayout(flow);
		for(int i=1; i<6; i++)
			content.add(new JButton("Spausk "+i));
		
		
	}
	
	public void createBorderLayout(){
		Container content = getContentPane();
		BorderLayout borderLayout = new BorderLayout(10, 10);
		content.setLayout(borderLayout);
		//content.add(new JButton("Spausk į Rytus"), BorderLayout.EAST);
		//content.add(new JButton("Spausk į Vakarus"), BorderLayout.WEST);
		content.add(new JButton("Spausk į Šiaurę"), BorderLayout.NORTH);
		content.add(new JButton("Spausk į Pietus"), BorderLayout.SOUTH);
		content.add(new JButton("Spausk į Vidurį"));
	}
	
	public void createGridLayout(){
		Container content = getContentPane();
		GridLayout gridLayout = new GridLayout(3, 4, 30, 20);
		content.setLayout(gridLayout);
		for(int i=1; i<10; i++)
			content.add(new JButton("Spausk "+i));
		
	}
	
	public void createBoxLayout(){
		Container content = getContentPane();
		
		head.add(textLabel);
		content.add(head, BorderLayout.NORTH);
		
		
		Box left = Box.createVerticalBox();
		
		JRadioButton button;
		ButtonGroup radioGroup = new ButtonGroup();
		
		button = new JRadioButton("Red");
		button.setForeground(Color.RED);
		left.add(Box.createVerticalStrut(30));
		left.add(button);
		radioGroup.add(button);

		button = new JRadioButton("Green");
		button.setForeground(new Color(14, 200, 30));
		left.add(Box.createVerticalStrut(30));
		left.add(button);
		radioGroup.add(button);
		
		button = new JRadioButton("Blue");
		button.setForeground(Color.BLUE);
		left.add(Box.createVerticalStrut(30));
		left.add(button);
		radioGroup.add(button);
		
		button = new JRadioButton("Yellow");
		button.setForeground(Color.YELLOW);
		left.add(Box.createVerticalStrut(30));
		left.add(button);
		radioGroup.add(button);
		left.add(Box.createVerticalStrut(30));
		left.add(Box.createVerticalGlue());
		
		Box right = Box.createVerticalBox();
		right.add(Box.createVerticalStrut(30));
		right.add(new JCheckBox("Dashed"));
		right.add(Box.createVerticalStrut(30));
		right.add(new JCheckBox("Thick"));
		right.add(Box.createVerticalStrut(30));
		right.add(new JCheckBox("Rounded"));
		right.add(Box.createVerticalGlue());
		
		JPanel leftPanel = new JPanel(new BorderLayout());
		leftPanel.setBorder(new TitledBorder(new EtchedBorder(), "Line Color"));
		leftPanel.add(left);
		
		JPanel rightPanel = new JPanel(new BorderLayout());
		rightPanel.setBorder(new TitledBorder(new EtchedBorder(), "Line Properties"));
		rightPanel.add(right);
		
		
		Box top = Box.createHorizontalBox();
		top.add(Box.createHorizontalStrut(5));
		top.add(leftPanel);
		top.add(Box.createHorizontalStrut(5));
		top.add(rightPanel);
		top.add(Box.createHorizontalStrut(5));

		content.add(top);

		
		JPanel bottom = new JPanel();
		Dimension size = new Dimension(80, 20);
		JButton b;
		bottom.add(b = new JButton("Defaults"));
		b.setPreferredSize(size);
		bottom.add(b = new JButton("OK"));
		b.setPreferredSize(size);
		bottom.add(b = new JButton("Cancel"));
		b.addActionListener(this);
		b.setPreferredSize(size);
		
		content.add(bottom, BorderLayout.SOUTH);
		
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		JMenu fileMenu = new JMenu("File");
		JMenu toolsMenu = new JMenu("Tools");
		menuBar.add(fileMenu);
		menuBar.add(toolsMenu);
		
		
		OpenListener openListener = new OpenListener(this);
		JMenuItem newItem = fileMenu.add("New");
		newItem.addActionListener(openListener);
		JMenuItem openItem = fileMenu.add("Open");
		openItem.addActionListener(openListener);
		JMenuItem closeItem = fileMenu.add("Close");
		closeItem.addActionListener(openListener);
		//closeItem.addMouseListener(new MyMouseListener(this));
		fileMenu.addSeparator();
		JMenuItem exitItem = fileMenu.add("Exit");
		exitItem.addActionListener(this);
		
		JRadioButtonMenuItem shapeItem;
		ButtonGroup shapeGroup = new ButtonGroup();
		toolsMenu.add(shapeItem = new JRadioButtonMenuItem("Line", true));
		shapeGroup.add(shapeItem);
		toolsMenu.add(shapeItem = new JRadioButtonMenuItem("Rectangle", false));
		shapeGroup.add(shapeItem);
		toolsMenu.add(shapeItem = new JRadioButtonMenuItem("Circle", false));
		shapeGroup.add(shapeItem);
		toolsMenu.add(shapeItem = new JRadioButtonMenuItem("Curve", false));
		shapeGroup.add(shapeItem);

		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		exit();
	}

}
