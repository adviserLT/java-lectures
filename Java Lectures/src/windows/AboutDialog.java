package windows;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class AboutDialog extends JDialog implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public AboutDialog(JFrame parent, String title, String message) {
		super(parent, title);
		if(parent != null){
			Dimension parentSize = parent.getSize();
			Point p = parent.getLocation();
			setLocation(p.x + parentSize.width/4, p.y + parentSize.height/4);
		}
		
		JPanel messagePanel = new JPanel();
		messagePanel.add(new JLabel(message));
		getContentPane().add(messagePanel);
		
		JPanel buttonPanel = new JPanel();
		JButton button = new JButton("OK");
		button.addActionListener(this);
		buttonPanel.add(button);
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		pack();
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		setVisible(false);
		dispose();
	}
	
	

}
