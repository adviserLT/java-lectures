package windows.generated;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.Box;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import java.awt.Component;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.ButtonGroup;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.Dimension;
import javax.swing.JSeparator;
import javax.swing.JRadioButtonMenuItem;

public class MyGeneratedWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel content;
	private final ButtonGroup colorButtonGroup = new ButtonGroup();
	private final ButtonGroup shapeButtonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyGeneratedWindow frame = new MyGeneratedWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyGeneratedWindow() {
		setTitle("Mano generuotas langas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		
		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				System.exit(0);
			}
		});
		
		JMenuItem newItem = new JMenuItem("New");
		fileMenu.add(newItem);
		
		JMenuItem openItem = new JMenuItem("Open");
		fileMenu.add(openItem);
		
		JMenuItem closeItem = new JMenuItem("Close");
		fileMenu.add(closeItem);
		
		JSeparator separator = new JSeparator();
		fileMenu.add(separator);
		fileMenu.add(exitItem);
		
		JMenu toolsMenu = new JMenu("Tools");
		menuBar.add(toolsMenu);
		
		JRadioButtonMenuItem lineRadioItem = new JRadioButtonMenuItem("Line");
		shapeButtonGroup.add(lineRadioItem);
		lineRadioItem.setSelected(true);
		toolsMenu.add(lineRadioItem);
		
		JRadioButtonMenuItem rectangleRadioItem = new JRadioButtonMenuItem("Rectangle");
		shapeButtonGroup.add(rectangleRadioItem);
		toolsMenu.add(rectangleRadioItem);
		
		JRadioButtonMenuItem circleRadioItem = new JRadioButtonMenuItem("Circle");
		shapeButtonGroup.add(circleRadioItem);
		toolsMenu.add(circleRadioItem);
		
		JRadioButtonMenuItem curveRadioItem = new JRadioButtonMenuItem("Curve");
		shapeButtonGroup.add(curveRadioItem);
		toolsMenu.add(curveRadioItem);
		content = new JPanel();
		content.setBorder(new EmptyBorder(5, 5, 5, 5));
		content.setLayout(new BorderLayout(0, 0));
		setContentPane(content);
		
		Box top = Box.createHorizontalBox();
		content.add(top, BorderLayout.CENTER);
		
		JPanel left = new JPanel();
		left.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Line Color", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		top.add(left);
		left.setLayout(new BorderLayout(0, 0));
		
		Box leftBox = Box.createVerticalBox();
		left.add(leftBox);
		
		Component verticalStrut = Box.createVerticalStrut(30);
		leftBox.add(verticalStrut);
		
		JRadioButton redRadioButton = new JRadioButton("Red");
		redRadioButton.setForeground(Color.RED);
		colorButtonGroup.add(redRadioButton);
		leftBox.add(redRadioButton);
		
		Component verticalStrut_1 = Box.createVerticalStrut(30);
		leftBox.add(verticalStrut_1);
		
		JRadioButton greenRadioButton = new JRadioButton("Green");
		greenRadioButton.setForeground(new Color(34, 139, 34));
		colorButtonGroup.add(greenRadioButton);
		leftBox.add(greenRadioButton);
		
		Component verticalStrut_2 = Box.createVerticalStrut(30);
		leftBox.add(verticalStrut_2);
		
		JRadioButton blueRadioButton = new JRadioButton("Blue");
		blueRadioButton.setForeground(Color.BLUE);
		colorButtonGroup.add(blueRadioButton);
		leftBox.add(blueRadioButton);
		
		Component verticalStrut_3 = Box.createVerticalStrut(30);
		leftBox.add(verticalStrut_3);
		
		JRadioButton yellowRadioButton = new JRadioButton("Yellow");
		yellowRadioButton.setForeground(new Color(255, 215, 0));
		colorButtonGroup.add(yellowRadioButton);
		leftBox.add(yellowRadioButton);
		
		Component verticalStrut_4 = Box.createVerticalStrut(30);
		leftBox.add(verticalStrut_4);
		
		Component verticalGlue_1 = Box.createVerticalGlue();
		leftBox.add(verticalGlue_1);
		
		JPanel right = new JPanel();
		right.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Line Properties", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		top.add(right);
		right.setLayout(new BorderLayout(0, 0));
		
		Box rightBox = Box.createVerticalBox();
		right.add(rightBox);
		
		Component verticalStrut_5 = Box.createVerticalStrut(30);
		rightBox.add(verticalStrut_5);
		
		JCheckBox dashedCheckBox = new JCheckBox("Dashed");
		rightBox.add(dashedCheckBox);
				
		Component verticalStrut_6 = Box.createVerticalStrut(30);
		rightBox.add(verticalStrut_6);
			
		JCheckBox ThickCheckBox = new JCheckBox("Thick");
		rightBox.add(ThickCheckBox);
				
		Component verticalStrut_7 = Box.createVerticalStrut(30);
		rightBox.add(verticalStrut_7);
				
		JCheckBox roundedCheckBox = new JCheckBox("Rounded");
		rightBox.add(roundedCheckBox);
				
		Component verticalGlue = Box.createVerticalGlue();
		rightBox.add(verticalGlue);
				
		JPanel bottom = new JPanel();
		content.add(bottom, BorderLayout.SOUTH);
			
		JButton defaultsButton = new JButton("Defaults");
		defaultsButton.setPreferredSize(new Dimension(80, 20));
		bottom.add(defaultsButton);
				
		JButton okButton = new JButton("OK");
		okButton.setPreferredSize(new Dimension(80, 20));
		bottom.add(okButton);
				
		JButton cancelButton = new JButton("Cancel");
		cancelButton.setPreferredSize(new Dimension(80, 20));
		bottom.add(cancelButton);

		pack();		
		setMinimumSize(getPreferredSize());
		setLocationRelativeTo(null);
		
	}

}
