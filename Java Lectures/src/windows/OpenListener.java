package windows;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OpenListener implements ActionListener {

	private MyWindow window;
	
	public OpenListener(MyWindow window) {
		super();
		this.window = window;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		window.invokeDialog(event.getActionCommand());
	}

}
