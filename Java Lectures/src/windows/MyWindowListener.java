package windows;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class MyWindowListener implements WindowListener {
	
	private MyWindow window;
	
	public MyWindowListener(MyWindow window) {
		this.window = window;
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		window.appendText("Fokusas aktyvuotas!");
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		window.appendText("Langas uždarytas!");
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		window.appendText("Langas uždaromas!");
		window.exit();
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		window.appendText("Fokusas deaktivuotas!");
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		window.appendText("Pakeltas!");
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		window.appendText("Nuleistas!");
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		window.appendText("Langas atidarytas!");
	}

}
