package windows;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MyMouseListener extends MouseAdapter {
	
	private MyWindow window;

	public MyMouseListener(MyWindow window) {
		this.window = window;
	}
	
	@Override
	public void mousePressed(MouseEvent event) {
		if(event.getButton() == MouseEvent.BUTTON1){
			window.appendText("Kairys mygtukas paspaustas!");
		}else if(event.getButton() == MouseEvent.BUTTON2){
			window.appendText("Vidurinis mygtukas paspaustas!");
		}else if(event.getButton() == MouseEvent.BUTTON3){
			window.appendText("Dešinys mygtukas paspaustas!");
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		String text = "("+e.getXOnScreen()+":"+e.getYOnScreen()+")";
		window.displayText(text);
	}

}
