package windows;

public class WindowThread implements Runnable {

	@Override
	public void run() {
		MyWindow window = new MyWindow("Mano pirmas langas");
		MyWindowListener windowListener = new MyWindowListener(window);
		MyMouseListener mouseListener = new MyMouseListener(window);
		window.addWindowListener(windowListener);
		window.addMouseListener(mouseListener);
		window.addMouseMotionListener(mouseListener);
		
		//window.setLocation(200, 200);
		//window.setSize(800, 600);
		
		window.createBoxLayout();

		
		
		window.pack();
		window.setMinimumSize(window.getPreferredSize());
		
		window.setLocationRelativeTo(null);
		
		/*Toolkit toolkit = window.getToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		window.setBounds(screenSize.width/4, screenSize.height/4, screenSize.width/2, screenSize.height/2);*/
		
		
		
		window.setVisible(true);
	}

}
