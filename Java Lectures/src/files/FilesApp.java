package files;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class FilesApp {

	public static void main(String[] args) {
		int bufferData[] = {0x4c, 0x41, 0x42, 0x41, 0x53};
		String bufferName = "file.bfr";
		try {
			writeByteFile(bufferName, bufferData);
		} catch (IOException e) {
			System.err.println("Failas tokiu pavadinimu '"+bufferName+"' nerastas arba trūksta teisių!");
		}
		
		try {
			readByteBuffer(bufferName);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		
		String stringData = "Lietuviškas tekstas";
		String stringName = "file.txt";
		try {
			writeStringFile(stringName, stringData);
		} catch (IOException e) {
			System.err.println("Failas tokiu pavadinimu '"+stringName+"' nerastas arba trūksta teisių!");
		}
		try {
			readStringBuffer(stringName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeByteFile(String fileName, int[] data) throws IOException{
		FileOutputStream myFileStream = new FileOutputStream(fileName);
		BufferedOutputStream myBufferedStream = new BufferedOutputStream(myFileStream);
		for (int i = 0; i < data.length; i++) {
			myBufferedStream.write(data[i]);
		}
		myBufferedStream.flush();
		myBufferedStream.close();
		myFileStream.close();
	}
	
	
	
	public static void writeStringFile(String fileName, String data) throws IOException{
		FileOutputStream myFileStream = new FileOutputStream(fileName);
		OutputStreamWriter myOutputWriter = new OutputStreamWriter(myFileStream, "ISO-8859-1");
		BufferedWriter myBufferedWriter = new BufferedWriter(myOutputWriter);
		myBufferedWriter.write(data);
		myBufferedWriter.flush();
		myBufferedWriter.close();
		myOutputWriter.close();
		myFileStream.close();
	}
	
	public static void readByteBuffer(String fileName) throws IOException{
		FileInputStream myFileStream = new FileInputStream(fileName);
		BufferedInputStream myInputBuffer = new BufferedInputStream(myFileStream);
		int chunk;
		do {
			chunk = myInputBuffer.read();
			System.out.println(chunk);
		} while (chunk != -1);
		
		myInputBuffer.close();
		myFileStream.close();
		
	}
	
	public static void readStringBuffer(String fileName) throws IOException {
		FileInputStream myFileStream = new FileInputStream(fileName);
		InputStreamReader myInputReader = new InputStreamReader(myFileStream, "UTF8");
		BufferedReader myReader = new BufferedReader(myInputReader);
		int ch;
		StringBuffer myBuffer = new StringBuffer();
		while ((ch = myReader.read()) > -1) {
			myBuffer.append((char)ch);
		}
		System.out.println(myBuffer);
		myReader.close();
		myInputReader.close();
		myFileStream.close();
	}
	

}
