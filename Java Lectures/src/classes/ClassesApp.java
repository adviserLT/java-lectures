package classes;

public class ClassesApp {

	public static void main(String[] args) {
		
		Employee petrasEmployee = new Employee();
		petrasEmployee.name = "Petras";
		petrasEmployee.salary = 300.0;
		
		Employee jonasEmployee = new Employee();
		jonasEmployee.name = "Jonas";
		jonasEmployee.salary = 2230.0;
		
		System.out.println(jonasEmployee.getSalaryText());
		
		Sphere ball = new Sphere();
		ball.xCenter = 4;
		ball.yCenter = 50;
		ball.zCenter = 34;
		ball.radius = 0.3;
		
		Sphere globe = new Sphere();
		globe.xCenter = 900;
		globe.yCenter = 3400;
		globe.zCenter = 7700;
		globe.radius = 10000;
		
		double volumeBall = 4/3 * Sphere.PI * Math.pow(ball.radius, 3);
		double volumeGlobe = 4/3 * Sphere.PI * Math.pow(globe.radius, 3);
		System.out.println(volumeBall);
		System.out.println(volumeGlobe);

	}

}
