package classes.exercises;

public class Student {
	
	public String name = "";
	
	public String surname = "";
	
	public int grade;
	
	public String getGradeText(){
		switch (grade) {
		case 1:
			return "1 (blogai)";
		case 2:
			return "2 (nepatenkinamai)";
		case 3:
			return "3 (silpnai)";
		case 4:
			return "4 (gerai)";
		case 5:
			return "5 (puikiai)";
		default:
			return "vertinimas neatpažintas";
		}
	}


}
