package classes.exercises;

public class StudentsApp {

	public static void main(String[] args) {
		
		Student [] students = new Student[3];
		Student student;
		
		student = new Student();
		student.name	= "Jonas";
		student.surname = "Jonaitis";
		student.grade = 6;
		students[0] = student;

		student = new Student();
		student.name = "Petras";
		student.surname = "Petraitis";
		student.grade = 2;
		students[1] = student;

		student = new Student();
		student.name = "Onutė";
		student.surname = "Jonaitienė";
		student.grade = 7;
		students[2] = student;
		
		for (int i = 0; i < students.length; i++) {
			System.out.println(students[i].name + " " + students[i].surname + " " + students[i].grade + " - " + students[i].getGradeText());
		}

	}

}
