package inheritance;

public class Cat extends Mammal {

	private String name;
	
	public Cat(String name) {
		super("Katinas");
		this.name = name;
	}
	
	@Override
	public String getStringRepresentation() {
		return super.getStringRepresentation() + ", vardu " + name;
	}
	
	@Override
	public void sound() {
		System.out.println("Miau, miau!");
	}

}
