package inheritance.exercises.statistics;

import java.util.Scanner;

public class Printer {

	private Scanner scanner;

	public Printer() {
		this.scanner = new Scanner(System.in);
	}
	
	public void print(String text){
		System.out.println(text);
	};

	public void print(Printable printable){
		print(printable.printResult());
	};
	
	public String read(String text){
		this.print(text);
		return this.scanner.nextLine();
	}
	
	public void close(){
		this.scanner.close();
	}
	
}
