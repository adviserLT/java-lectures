package inheritance.exercises.statistics;

import java.util.List;

public abstract class Action {

	public abstract double calculate(List<Integer> data);
	
}
