package inheritance.exercises.statistics;

import java.util.Iterator;
import java.util.List;

public class Max extends Action {

	@Override
	public double calculate(List<Integer> data) {
		Iterator<Integer> iterator = data.iterator();
		int min = iterator.next();
		while (iterator.hasNext()) {
			min = Math.max(min, iterator.next()) ;
		}
		return min;
	}

}
