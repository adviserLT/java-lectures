package inheritance.exercises.statistics;

import java.util.Comparator;
import java.util.List;

public class Median extends Action {

	@Override
	public double calculate(List<Integer> data) {
		int index = (int) (data.size()/2);

		data.sort(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o1.compareTo(o2);
			}
		});
		
		if(data.size() > 1 && data.size() % 2 == 0){
			return ((double)(data.get(index-1) + data.get(index)))/2;
		}else{
			return data.get(index);
		}
	}

}
