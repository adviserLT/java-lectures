package inheritance.exercises.statistics;

public interface Printable {
	
	public static final String MEAN = "mean";
	public static final String MAX = "max";
	public static final String MEDIAN = "median";
	public static final String MIN = "min";

	public String printResult();
	
}
