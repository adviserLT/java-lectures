package inheritance.exercises.statistics;

public class StatisticsApp {

	public static void main(String[] args) {
		Printer printer = new Printer();
		printer.print("## STATISTIKOS PROGRAMA ##");
		
		Result result = new Result(printer);
		result.readData();
		result.chooseAction();
		
		printer.print(result);
		printer.close();
	}

}
