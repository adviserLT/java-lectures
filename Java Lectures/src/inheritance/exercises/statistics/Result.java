package inheritance.exercises.statistics;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Result implements Printable {

	private Action [] allActions = new Action[4];
	
	private Printer printer;
	
	
	private Action currentAction = null;
	
	private List<Integer> data = new ArrayList<Integer>();
	
	public Result(Printer printer) {
		this.printer = printer;
		allActions[0] = new Mean();
		allActions[1] = new Max();
		allActions[2] = new Median();
		allActions[3] = new Min();
	}

	public void readData(){
		String dataLine = printer.read("Įveskite skaičių seką atskirdami skaičius tarpu:");
		StringTokenizer spaceTokenizer = new StringTokenizer(dataLine, " ");
		while (spaceTokenizer.hasMoreElements()) {
			String number = (String) spaceTokenizer.nextElement();
			try {
				data.add(Integer.valueOf(number));
			} catch (NumberFormatException e) {
				printer.print("KLAIDA: sekoje yra ne tik skaičių!");
			}
		}
	}
	
	public void chooseAction(){
		String actionToken = printer.read("Įveskite komandą 'mean, max, median, min':");
		if(actionToken.equals(Printable.MEAN)){
			currentAction = allActions[0];
		}else if(actionToken.equals(Printable.MAX)){
			currentAction = allActions[1];
		}else if(actionToken.equals(Printable.MEDIAN)){
			currentAction = allActions[2];
		}else if(actionToken.equals(Printable.MIN)){
			currentAction = allActions[3];
		}else{
			currentAction = null;
		}
	}
	
	@Override
	public String printResult() {
		if(currentAction != null){
			if(data.size()>0){
				double result = currentAction.calculate(data);
				return "Gautas rezultatas: "+ result;
			}else{
				return "KLAIDA: per mažai duomenų!";
			}
		}else{
			return "KLAIDA: nebuvo pasirinkta operacija!";
		}

	}
	

}
