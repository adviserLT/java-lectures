package inheritance.exercises.statistics;

import java.util.Iterator;
import java.util.List;

public class Mean extends Action {

	@Override
	public double calculate(List<Integer> data) {
		Iterator<Integer> iterator = data.iterator();
		int sum = 0;
		while (iterator.hasNext()) {
			sum += iterator.next();
		}
		return ((double)sum) / data.size();
	}

}
