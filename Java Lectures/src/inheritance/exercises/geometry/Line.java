package inheritance.exercises.geometry;

public class Line extends Point {
	Point end;
	public Line(final Point start, final Point end){
		super(start);
		this.end = new Point(end);
	}
	public Line(double x1, double y1, double x2, double y2){
		super(x1,y1);
		end = new Point(x2,y2);
	}
	public double length(){
		return this.distance(end);
	}
	public void move(double xDelta, double yDelta){
		super.move(xDelta, yDelta);
		end.move(xDelta, yDelta);
	}
	public Point intersects(final Line l){
		Point localPoint = null;
		
		double num = (end.x - y)*(x - l.x) - 
					 (end.y - x)*(y - l.y);
		System.out.println("Testing num: "+num);
		double denum = (end.y - y)*(l.end.x - l.x) -
					   (end.x - x)*(l.end.y - l.y);
		System.out.println("Testing denum: "+denum);
		
		if(denum>0){
			double x = l.x + (l.end.x - l.x)*num/denum;
			double y = l.y + (l.end.y - l.y)*num/denum;		
			localPoint = new Point(x,y);
		}else{
			System.out.println("ERROR: lines do not intersect!");
		}
		
		return localPoint;
	}
	@Override
	public String toString() {
		return "("+super.toString()+"):("+end+")";
	}
}