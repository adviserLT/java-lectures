package inheritance.exercises.geometry;

public class ShapeApp {

	public static void main(String[] args) {
		Point p1 = new Point(5,6);
		Point p2 = new Point(10,60);
		Line line = new Line(p1, p2);
		Line line2 = new Line(p1, p2);
		
		p1.move(5, -5);
		System.out.println("Calculating distance for points: ("+p1+"), ("+p2+"):");
		System.out.println(p1.distance(p2));
		
		line.move(3, 4);
		System.out.println("Line "+line+" length:");
		System.out.println(line.length());
		
		
		System.out.println("Lines intersect at point:");
		System.out.println(line.intersects(line2));

	}

}
