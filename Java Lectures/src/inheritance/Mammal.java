package inheritance;

public abstract class Mammal extends Animal {

	public Mammal(String type) {
		super(type);
	}

	public void produceHeat() {
		System.out.println("Šildymas...");
	}
	
}
