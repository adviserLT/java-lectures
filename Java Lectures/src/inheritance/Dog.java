package inheritance;

public class Dog extends Mammal {

	private String name;
	private String breed;
	
	public Dog(String name) {
		this(name, "Unknown");
	}
	
	public Dog(String name, String breed) {
		super("Šuo");
		this.name = name;
		this.breed = breed;
	}
	
	@Override
	public String getStringRepresentation() {
		return super.getStringRepresentation() + ", vardas - " + name + ", veslė - " + breed;
	}
	
	@Override
	public void sound() {
		System.out.println("Au, au!");
	}

}
