package inheritance;

import java.util.Random;

public class InheritanceApp {

	public static void main(String[] args) {
		Animal [] animals = {
			new Dog("Brisius", "Čihuahua"),
			new Dog("Lassie"),
			new Cat("Garfildas"),
			new Duck()
		};
		//Animal duck = new Animal("Antis");
		//brisius = duck;

		Animal unknown;
		Random select = new Random();
		for (int i = 0; i < 5; i++) {
			unknown = animals[select.nextInt(animals.length)];
			System.out.println(unknown);
			unknown.sound();
			
			if(unknown instanceof EggLayingAnimal){
				EggLayingAnimal needEggs = (EggLayingAnimal) unknown;
				needEggs.layEgg();
			}else if(unknown instanceof Mammal){
				useMammal((Mammal) unknown);
			}
			
		}
		
	}
	
	public static void useMammal(Mammal mammal){
		mammal.produceHeat();
	}

}
