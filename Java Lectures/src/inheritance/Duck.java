package inheritance;

public class Duck extends Animal implements EggLayingAnimal {

	public Duck() {
		super("Antis");
	}

	@Override
	public void layEgg() {
		System.out.println(type + " padėjo kiaušinį!");

	}

	@Override
	public void sound() {
		System.out.println("Kre, kre!");
	}

}
